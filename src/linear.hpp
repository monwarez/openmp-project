/*
Copyright (c) 2016, Payet Thibault
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Payet Thibault nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL PAYET THIBAULT BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#pragma once
#include <omp.h>
#include <vector>
#include <string>
#include <iostream>

template<class T>
class Matrice
{
	public:
		Matrice(unsigned int m, unsigned int n);
		std::vector < std::vector <T>> const & GetMatrice() const;
		void	ShowMatrice();
		Matrice<T> & operator+(Matrice<T> const &m);
		void	LoadIdentity(T const & identity, T const &zero);
	protected:
		std::vector < std::vector<T>>	m_data;
		unsigned int					m_m;
		unsigned int					m_n;

};

template <class T>
Matrice<T>::Matrice(unsigned int m, unsigned int n):m_m(m), m_n(n)
{
	m_data.resize(m);
	// No need to parallel this
	for (unsigned int i=0; i < m; ++i)
		m_data[i].resize(n);
}

template <class T>
Matrice<T> & Matrice<T>::operator+(Matrice<T> const & a)
{
	if ((a.m_m != m_m) || (a.m_n != m_n))
		throw std::string ("Error of dimension");
	unsigned n(a.m_n),m(a.m_m);
	Matrice<T> *pB;
	pB		=	this;
	unsigned int i(0),j(0);
	#pragma omp parallel shared(m,n,a,pB)  private (i,j)
	{
		#pragma omp for schedule(dynamic) nowait
			for (i = 0; i < m; ++i)
				for (j = 0; j < n ;++j)
					pB->m_data[i][j] += a.m_data[i][j] ;
	}
	return *this;
}
template <class T>
void	Matrice<T>::ShowMatrice()
{
	for (unsigned int i=0; i< m_m; ++i)
	{
		for (unsigned int j=0; j < m_n; ++j)
			std::cout << m_data[i][j] << '\t';
		std::cout << std::endl;
	}
}
template <class T>
void	Matrice<T>::LoadIdentity(T const & identity, T const & zero)
{
	unsigned int n(m_n),m(m_m);
	unsigned int i(0),j(0);
	auto	p=this;
	#pragma omp parallel shared(m,n,p,identity,zero) private(i,j)
	{
		#pragma omp for schedule(dynamic) nowait
		for (i=0; i < m; ++i)
			for (j=0; j < n; ++j)
				p->m_data[i][j]	=	(i==j)?identity:zero;
	}
}
template <class T>
std::vector<std::vector<T>> const & Matrice<T>::GetMatrice() const
{
	return  m_data;
}
