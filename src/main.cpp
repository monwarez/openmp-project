/*
Copyright (c) 2016, Payet Thibault
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Payet Thibault nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL PAYET THIBAULT BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include <omp.h>
#include <iostream>

#include "linear.hpp"

int main(int argc, char ** argv)
{
	std::cout << "Number of threads: " << *omp_get_num_threads << std::endl;
	int nthreads, tid;
	#pragma omp parallel private(tid)
	{
		tid	=	omp_get_thread_num();
		std::cout << "Hi from thread = " << tid << std::endl;
	
		if (0 == tid)
		{
			nthreads	=	omp_get_num_threads();
			std::cout << "Number of threads: " << nthreads << std::endl;
		}
	}
	std::cout << "End hello world" << std::endl;

	Matrice<double> a(4,4);
	Matrice<double>	b(4,4);
	a.LoadIdentity(2.0,0.0);
	b.LoadIdentity(7.0,0.0);
	std::cout << "first matrice" << std::endl;
	a.ShowMatrice();
	std::cout << "second matrice" << std::endl;
	b.ShowMatrice();
	auto	c	=	a + b;
	std::cout << "Result of a + b " << std::endl;
	c.ShowMatrice();
	
	return EXIT_SUCCESS;
}
